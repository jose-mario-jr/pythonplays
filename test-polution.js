const industries = [2, 5, 7, 10]

const sum = a => a.reduce((acc, b) => b + acc)
totalInitial = sum(industries)
console.log(totalInitial)

const withFilters = [...industries]
const filtersArray = industries.map(_ => 0)

while (sum(withFilters) > totalInitial / 2) {
  Math.max(...withFilters)
  maxIndex = withFilters.findIndex(e => e === Math.max(...withFilters))
  withFilters[maxIndex] /= 2
  filtersArray[maxIndex] += 1
  console.log(withFilters)
  console.log(filtersArray)
}

console.log("Total Filters: ", sum(filtersArray))
