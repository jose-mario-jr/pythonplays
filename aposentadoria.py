
#  output do commit 676af0d8: 

#    para 500 mensais e uma aposentadoria de 5000 
#  montante = 1000724.0940013933, total de juros: 760724.0940013942, 
#         meses: 480, anos: 40 anos.

#    para 1000 mensais e uma aposentadoria de 5000
#  montante = 1003515.0424526426, total de juros: 644515.042452643, 
#         meses: 359, anos: 29 anos e 11 meses.

#    para 1000 mensais e uma aposentadoria de 2500
#  montante = 501874.12893986463, total de juros: 250874.12893986472, 
#         meses: 251, anos: 20 anos e 11 meses.

#    para 500 mensais e uma aposentadoria de 2500
#  montante = 501757.5212263213, total de juros: 322257.5212263215, 
#         meses: 359, anos: 29 anos e 11 meses.

#    para 5000 mensais e uma aposentadoria de 5000
#  montante = 1005243.4045802854, total de juros: 310243.40458028554, 
#         meses: 139, anos: 11 anos e 7 meses.

#    para 1000 mensais e uma aposentadoria de 10000
#  montante = 2001448.1880027866, total de juros: 1521448.1880027885,
#         meses: 480, anos: 40 anos.

#  output do atual: 

#    para 200 mensais que aumentam 20% do valor inicial a cada ano, desejando aposentadoria de 5000:
#  montante = 1004341.6052298733, total de juros: 617141.6052298729, 
#         meses: 431, anos: 35 anos e 11 meses.


 

def paraAnos(periodoMeses):

  anos = int(periodoMeses/12)

  meses = periodoMeses % 12

  adicional = '.' if meses == 0 else ' e ' + str(meses) + ' meses.'

  return '' + str(anos) + ' anos' + adicional

adicaoMensalInicial = 200
adicaoMensal = adicaoMensalInicial
salarioDesejado = 5000

salario = 0
periodoMeses = 0
jurosMensal = 0.005  # pensando em 6% anuais da poupança nubank e inter

montante = 0
jurosEsteMes = 0
totalJuros = 0


while salario < salarioDesejado:
  montante += adicaoMensal
  jurosEsteMes = montante * jurosMensal
  montante += jurosEsteMes
  totalJuros += jurosEsteMes

  periodoMeses += 1

  salario = montante * jurosMensal

  if periodoMeses % 12 == 0: 
    adicaoMensal += adicaoMensalInicial * 0.2
    print('passou um ano' + str(periodoMeses/12) + 'agora vai pra: ' + str(adicaoMensal))



print('montante = '+ str(montante) + ', total de juros: ' + str(totalJuros) + 
  ', meses: ' + str(periodoMeses) + ', anos: ' + paraAnos(periodoMeses) )
