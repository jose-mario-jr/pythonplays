const { pigTranslator } = require('./translator.js')

const tests = [
  { input: 'Stop', output: 'Opstay' },
  { input: 'No littering', output: 'Onay itteringlay' },
  {
    input: 'No shirts, no shoes, no service',
    output: 'Onay irtsshay, onay oesshay, onay ervicesay',
  },
  {
    input: 'No persons under 14 admitted',
    output: 'Onay ersonspay underay 14 admitteday',
  },
  {
    input: 'Hey buddy, get away from my car!',
    output: 'Eyhay uddybay, etgay awayay omfray ymay arcay!',
  },
]

const test = () => {
  let testsFailed = 0
  tests.forEach(({ input, output }) => {
    const words = input.split(' ')
    const translations = words.map(word => pigTranslator(word))
    const result = translations.join(' ')

    if (result === output) console.log('Test Passed!')
    else {
      console.log(`Test failed!\nExpected:\n${output}\ngot:\n${result}`)
      testsFailed += 1
    }
  })
  console.log(
    `Finished Testing, ${tests.length - testsFailed} tests were successfull ${
      testsFailed ? `, the other ${testsFailed} were not!` : ''
    }`
  )
}
module.exports = { test }
