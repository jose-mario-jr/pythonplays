const pigTranslator = word => {
  const vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y']
  const punctuationSigns = ['!', '.', ',', '?', ':', ';']

  // 1. If a word has no letters, don't translate it.
  if (word.length === 0) return ''
  if (parseInt(word)) return word

  // 4. Word is two parts. First is the “prefix” and extends from the beginning of the
  //    word up to, but not including, the first vowel. (The letter “y” will be
  //    considered a vowel.). The Rest of the word is called the “stem.”
  let prefix = ''
  let stem = ''
  let punctuation = ''
  let suffix = ''
  let hasConsonant = false

  for (let i = 0; i < word.length; i++) {
    if (stem === '' && !vowels.includes(word[i])) {
      prefix = `${prefix}${word[i]}`
    } else if (punctuationSigns.includes(word[i])) {
      // 2. All punctuation should be preserved.
      punctuation = `${punctuation}${word[i]}`
    } else {
      stem = `${stem}${word[i]}`
    }
    if (!vowels.includes(word[i])) {
      hasConsonant = true
    }
  }

  // 6. If the word contains no consonants, let the prefix be empty and the
  //    word be the stem. The word ending should be “yay” instead of merely
  //    “ay.” For example, “I” would be “Iyay.”
  suffix = hasConsonant ? 'ay' : 'yay'

  // 5. The translated text is formed by reversing the order of the prefix and stem and adding the letters “ay” to the
  //    end. For example, “sandwich” is composed of “s” + “andwich” + “ay” + “.” and would translate to
  //    “andwichsay.”
  let translation = `${stem}${prefix}${suffix}${punctuation}`

  // 3. If the word begins with a capital letter, then the translated word should too.
  if (word[0].toUpperCase() === word[0]) {
    translation =
      translation[0].toUpperCase() + translation.substring(1).toLowerCase()
  }

  return translation
}

module.exports = { pigTranslator }
