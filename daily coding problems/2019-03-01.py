#!/usr/bin/env python3

"""
Good morning! Here's your coding interview problem for today.

This problem was asked by Google.

Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s), which deserializes the string back into the tree.

For example, given the following Node class

class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
The following test should pass:

node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'

"""


class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
   
def serialize(root):
    string = '(' + root.val + '('
    if (root.left!=None):
        string = string + serialize(root.left)
    string += ')('
    if (root.right!=None):
        string = string + serialize(root.right)
    string += ')'
    return string
    
def deserialize(s):
    string = list(s)
    captWord = False
    end = False
    for item in string:
        if(captWord):
            if(item==')' or item == '('):
                captWord = False
                end = True
            else: 
                word = word + str(item)
                print(item, word)
               
            if(end):
                node = Node(word)
                print(word)
        else if(item=='('):    
            captWord = True
            end = False
            word = ''
        
            
    return Node

#The following test should pass:

node = Node('root', Node('left', Node('left.left')), Node('right'))

string = serialize(node)
print(string)
node = deserialize(string)
#assert deserialize(serialize(node)).left.left.val == 'left.left'
