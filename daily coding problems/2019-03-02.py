#!/usr/bin/env python3

"""

Good morning! Here's your coding interview problem for today.

This problem was asked by Stripe.

Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

You can modify the input array in-place.

"""

def firstMissing(listNumbers):
    listProcess = listNumbers.copy()
    retNum = 1
    end = False
    while end==False:
        try:
            listProcess.remove(retNum)
            retNum += 1
        except ValueError:
            end = True
            pass
    return retNum


listNumbers = []
num = 1
while(num != 0):
   num = int(input("Enter a number to the list, 0 to quit: "))
   if(num!=0): listNumbers.append(num)

print('First missing in list:', firstMissing(listNumbers))


