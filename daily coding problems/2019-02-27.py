#!/usr/bin/env python3  
# -*- coding: utf-8 -*-

def TrueFalse(k, listNumbers):
   greaterNumber = listNumbers[0]

   for num in listNumbers:
      if(num>greaterNumber): greaterNumber = num

   listNumbers.remove(greaterNumber)
   secondGreater = listNumbers[0]

   for num in listNumbers:
      if(num>secondGreater): secondGreater = num
      
   if (greaterNumber + secondGreater)>k: return True
   else: return False


k = int(input("k: "))

listNumbers = []
num = 1
while(num != 0):
   num = int(input("Enter a number to the list, 0 to quit"))
   if(num!=0): listNumbers.append(num)
   
print(TrueFalse(k, listNumbers))

"""
 Good morning! Here's your coding interview problem for today.

This problem was recently asked by Google.

Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

Bonus: Can you do this in one pass?
"""