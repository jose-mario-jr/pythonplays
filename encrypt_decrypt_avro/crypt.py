import csv
from cryptography.fernet import Fernet
from pyspark.context import SparkContext
from awsglue.context import GlueContext

sc = SparkContext()

glueContext = GlueContext(sc)
spark = glueContext.spark_session


def create_key_file() -> None:
    key = Fernet.generate_key()
    with open(".key", "wb") as filekey:
        filekey.write(key)


def get_fernet_key() -> Fernet:
    with open(".key", "rb") as filekey:
        key = filekey.read()

    return Fernet(key)


def csv_to_avro() -> None:
    df = spark.read.options(inferSchema="True", delimiter=",").csv("file.csv")

    df.write.format("avro").save("file.avro")


def encrypt_file(origin, target) -> None:

    fernet = get_fernet_key()

    with open(origin, "rb") as file:
        original = file.read()

    encrypted = fernet.encrypt(original)

    with open(target, "wb") as encrypted_file:
        encrypted_file.write(encrypted)


def decrypt_file(origin, target) -> None:

    fernet = get_fernet_key()

    with open(origin, "rb") as file:
        encrypted = file.read()

    decrypted = fernet.decrypt(encrypted)

    with open("file_decrypted.csv", "wb") as decrypted_file:
        decrypted_file.write(decrypted)


def main() -> None:

    # first create a key:
    # create_key_file()

    # then do whatever

    # encrypt_file('file.avro', 'file_encrypted.avro')

    # decrypt_file('file_encrypted.avro', 'file_decrypted.avro')

    csv_to_avro()


if __name__ == "__main__":
    main()  # pragma: no cover
