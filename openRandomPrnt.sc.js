const randomLetter = () => {
  const letters = 'abcdefghijklmnopqrstuvwxyz'
  return letters[Math.floor(Math.random() * letters.length)]
}
const randomNumber = () => {
  const numbers = '0123456789'
  return numbers[Math.floor(Math.random() * numbers.length)]
}
const randomImageId = () => {
  return `${randomLetter()}${randomLetter()}${randomNumber()}${randomNumber()}${randomNumber()}${randomNumber()}`
}

// open it and run in the same tab and it will open it, running again will make it refresh the page with the new image
window.open(`https://prnt.sc/${randomImageId()}`, '_self')

// const letters = 'abcdefghijklmnopqrstuvwxyz'
// const numbers = '0123456789'

// window.open(
//   `https://prnt.sc/${letters[Math.floor(Math.random() * letters.length)]}${
//     letters[Math.floor(Math.random() * letters.length)]
//   }${numbers[Math.floor(Math.random() * numbers.length)]}${
//     numbers[Math.floor(Math.random() * numbers.length)]
//   }${numbers[Math.floor(Math.random() * numbers.length)]}${
//     numbers[Math.floor(Math.random() * numbers.length)]
//   }`,
//   '_self'
// )
