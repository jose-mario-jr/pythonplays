
// select menu (... button)
const pt1 = () => {
  setTimeout(() => {
    document.querySelector('.hu5pjgll.m6k467ps').click()
    pt2()
  }, 50)
}
// click on delete messages
const pt2 = () => {
  setTimeout(() => {
    ;[...document.querySelectorAll('span')]
      .filter(e => e.innerText == 'Excluir bate-papo')[0]
      .click()
    pt3()
  }, 50)
}

// click on confirmation box
const pt3 = () => {
  setTimeout(() => {
    ;[...document.querySelectorAll('span')]
      .filter(e => e.innerText == 'Excluir bate-papo')[3]
      .click()
  }, 50)
}

//to make it all robust
const deleteAll = async () => {
  try {
    pt1()
  } catch (e) {
    console.log('erro')
  }
}


//execute
for (i = 0; i < 1000; i++) {
  setTimeout(() => {
      deleteAll()
  }, 200*i)
}
